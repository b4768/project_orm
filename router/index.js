const express = require("express");
const router = express.Router();
const categoryRouter = require("./category");
const userRouter = require("./user");

router.get("/check-health", (req, res) => res.send("Application Up"));
router.use("/category", categoryRouter);
router.use("/user", userRouter);

module.exports = router;
